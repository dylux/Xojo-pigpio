#tag Class
Class PigpioException
Inherits RuntimeException
	#tag Method, Flags = &h0
		Sub Constructor(message As String, errorCode As Integer = 0)
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From RuntimeException
		  // Constructor(message As String, errorCode As Integer = 0) -- From RuntimeException
		  Super.Constructor
		  
		  If message = "" Then
		    // Set the message if it's not already set.
		    
		    Self.ErrorNumber = errorCode
		    
		    SetMessageFromErrorCode
		    
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetMessageFromErrorCode()
		  Select Case Self.ErrorNumber
		  Case 0 
		    Self.Message = "Unknown error"
		  Case -1
		    Self.Message = "gpioInitialise failed"
		  Case -2
		    Self.Message = "GPIO not 0-31"
		  Case -3
		    Self.Message = "GPIO not 0-53"
		  Case -4
		    Self.Message =  "mode not 0-7"
		  Case -5
		    Self.Message = "level not 0-1"
		  Case -6
		    Self.Message =  "pud not 0-2"
		  Case -7
		    Self.Message =  "pulsewidth not 0 or 500-2500"
		  Case -8
		    Self.Message =  "dutycycle outside set range"
		  Case -9
		    Self.Message =  "timer not 0-9"
		  Case -10
		    Self.message ="ms not 10-60000"
		  Case -11
		    Self.Message =  "timetype not 0-1"
		  Case -12
		    Self.Message =  "seconds < 0"
		  Case -13
		    Self.message = "GPmicros not 0-999999"
		  Case -14
		    Self.Message =  "gpioSetTimerFunc failed"
		  Case -15
		    Self.message = "timeout not 0-60000"
		  Case -16, -18, -21
		    Self.Message = "DEPRECATED"
		  Case -17
		    Self.message = "clock peripheral not 0-1"
		  Case -19
		    Self.Message = "clock micros not 1, 2, 4, 5, 8, or 10"
		  Case -20
		    Self.message = "buf millis not 100-10000"
		  Case -21
		    Self.message = "dutycycle range not 25-40000"
		  Case -22
		    Self.Message = "signum not 0-63"
		  Case -23
		    Self.Message = "can't open pathname"
		  Case -24
		    Self.Message = "no handle available"
		  Case -25
		    Self.Message = "unknown handle"
		  Case -26
		    Self.Message = "ifFlags > 3"
		  Case -27
		    Self.Message = "DMA (primary) channel not 0-14"
		  Case -28
		    Self.Message = "socket port not 1024-32000"
		  Case -29
		    Self.Message = "unrecognized fifo command"
		  Case -30
		    Self.Message = "DMA secondary channel not 0-6"
		  Case -31
		    Self.Message = "function called before gpioInitialise"
		  Case -32
		    Self.Message = "function called after gpioInitialise"
		  Case -33
		    Self.Message = "waveform mode not 0-3"
		  Case -34
		    Self.Message = "bad parameter in gpioCfgInternals call"
		  Case -35
		    Self.Message = "baud rate not 50-250K(RX)/50-1M(TX)"
		  Case -36
		    Self.Message = "waveform has too many pulses"
		  Case -37
		    Self.Message = "waveform has too many chars"
		  Case -38
		    Self.Message = "no bit bang serial read in progress on GPIO"
		  Case -39
		    Self.Message = "bad (null) serial structure parameter"
		  Case -40
		    Self.Message = "bad (null) serial buf parameter"
		  Case -41
		    Self.Message = "GPIO operation not permitted"
		  Case -42
		    Self.Message = "one or more GPIO not permitted"
		  Case -43
		    Self.Message = "bad WVSC subcommand"
		  Case -44
		    Self.Message = "bad WVSM subcommand"
		  Case -45
		    Self.Message = "bad WVSP subcommand"
		  Case -46
		    Self.Message = "trigger pulse length not 1-100"
		  Case -47
		    Self.Message = "invalid script"
		  Case -48
		    Self.Message = "unknown script id"
		  Case -49
		    Self.Message = "add serial data offset > 30 minutes"
		  Case -50
		    Self.Message = "GPIO already in use"
		  Case -51
		    Self.Message = "must read at least a byte at a time"
		  Case -52
		    Self.Message = "script parameter id not 0-9"
		  Case -53
		    Self.Message = "script has duplicate tag"
		  Case -54
		    Self.Message = "script has too many tags"
		  Case -55
		    Self.Message = "illegal script command"
		  Case -56
		    Self.Message = "script variable id not 0-149"
		  Case -57
		    Self.Message = "no more room for scripts"
		  Case -58
		    Self.Message = "can't allocate temporary memory"
		  Case -59
		    Self.Message = "socket read failed"
		  Case -60
		    Self.Message = "socket write failed"
		  Case -61
		    Self.Message = "too many script parameters (> 10)"
		  Case -62
		    Self.Message = "script initialising"
		  Case -63
		    Self.Message = "script has unresolved tag"
		  Case -64
		    Self.Message = "bad MICS delay (too large)"
		  Case -65
		    Self.Message = "bad MILS delay (too large)"
		  Case -66
		    Self.Message = "non existent wave id"
		  Case -67
		    Self.Message = "No more CBs for waveform"
		  Case -68
		    Self.Message = "No more OOL for waveform"
		  Case -69
		    Self.Message = "attempt to create an empty waveform"
		  Case -70
		    Self.Message = "no more waveforms"
		  Case -71
		    Self.Message = "can't open I2C device"
		  Case -72
		    Self.Message = "can't open serial device"
		  Case -73
		    Self.Message = "can't open SPI device"
		  Case -74
		    Self.Message = "bad I2C bus"
		  Case -75
		    Self.Message = "bad I2C address"
		  Case -76
		    Self.Message = "bad SPI channel"
		  Case -77
		    Self.Message = "bad i2c/spi/ser open flags"
		  Case -78
		    Self.Message = "bad SPI speed"
		  Case -79
		    Self.Message = "bad serial device name"
		  Case -80
		    Self.Message = "bad serial baud rate"
		  Case -81
		    Self.Message = "bad i2c/spi/ser parameter"
		  Case -82
		    Self.Message = "i2c write failed"
		  Case -83
		    Self.Message = "i2c read failed"
		  Case -84
		    Self.Message = "bad SPI count"
		  Case -85
		    Self.Message = "ser write failed"
		  Case -86
		    Self.Message = "ser read failed"
		  Case -87
		    Self.Message = "ser read no data available"
		  Case -88
		    Self.Message = "unknown command"
		  Case -89
		    Self.Message = "spi xfer/read/write failed"
		  Case -90
		    Self.Message = "bad (NULL) pointer"
		  Case -91
		    Self.Message = "no auxiliary SPI on Pi A or B"
		  Case -92
		    Self.Message = "GPIO is not in use for PWM"
		  Case -93
		    Self.Message = "GPIO is not in use for servo pulses"
		  Case -94
		    Self.Message = "GPIO has no hardware clock"
		  Case -95
		    Self.Message = "GPIO has no hardware PWM"
		  Case -96
		    Self.Message = "hardware PWM frequency not 1-125M"
		  Case -97
		    Self.Message = "hardware PWM dutycycle not 0-1M"
		  Case -98
		    Self.Message = "hardware clock frequency not 4689-250M"
		  Case -99
		    Self.Message = "need password to use hardware clock 1"
		  Case -100
		    Self.Message = "illegal, PWM in use for main clock"
		  Case -101
		    Self.Message = "serial data bits not 1-32"
		  Case -102
		    Self.Message = "serial (half) stop bits not 2-8"
		  Case -103
		    Self.Message = "socket/pipe message too big"
		  Case -104
		    Self.Message = "bad memory allocation mode"
		  Case -105
		    Self.Message = "too many I2C transaction segments"
		  Case -106
		    Self.Message = "an I2C transaction segment failed"
		  Case -107
		    Self.Message = "SMBus command not supported by driver"
		  Case -108
		    Self.Message = "no bit bang I2C in progress on GPIO"
		  Case -109
		    Self.Message = "bad I2C write length"
		  Case -110
		    Self.Message = "bad I2C read length"
		  Case -111
		    Self.Message = "bad I2C command"
		  Case -112
		    Self.Message = "bad I2C baud rate, not 50-500k"
		  Case -113
		    Self.Message = "bad chain loop count"
		  Case -114
		    Self.Message = "empty chain loop"
		  Case -115
		    Self.Message = "too many chain counters"
		  Case -116
		    Self.Message = "bad chain command"
		  Case -117
		    Self.Message = "bad chain delay micros"
		  Case -118
		    Self.Message = "chain counters nested too deeply"
		  Case -119
		    Self.Message = "chain is too long"
		  Case -120
		    Self.Message = "deprecated function removed"
		  Case -121
		    Self.Message = "bit bang serial invert not 0 or 1"
		  Case -122
		    Self.Message = "bad ISR edge value, not 0-2"
		  Case -123
		    Self.Message = "bad ISR initialisation"
		  Case -124
		    Self.Message = "loop forever must be last chain command"
		  Case -125
		    Self.Message = "bad filter parameter"
		  Case -2000
		    Self.Message = "PI_PIGIF_ERR_0"
		  Case -2099
		    Self.Message = "PI_PIGIF_ERR_99"
		  Case -3000
		    Self.Message = "PI_CUSTOM_ERR_0"
		  Case -3999
		    Self.Message = "PI_CUSTOM_ERR_999"
		  End Select
		  
		End Sub
	#tag EndMethod


End Class
#tag EndClass
